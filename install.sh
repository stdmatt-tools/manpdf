#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : manpdf                                                        ##
##  Date      : Aug 13, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2018 - 2020                                         ##
##                                                                            ##
##  Description :                                                             ##
##    Just a simple installation script for manpdf.                           ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
INSTALL_BIN_DIR="/usr/local/bin";
INSTALL_SHARE_DIR="/usr/local/share";
PW_SHELLSCRIPT_UTILS_DIR="/usr/local/src/stdmatt/shellscript_utils/main.sh"


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
_install_source_on()
{
    local PATH_TO_INSTALL="$1";
    echo "Installing manpdf-aliases on ($PATH_TO_INSTALL)";

    if [ ! -e "$PATH_TO_INSTALL" ]; then
        pw_log_fatal "$PATH_TO_INSTALL does not exists - Aborting...";
    fi;

    local SHARE_DIR="$INSTALL_SHARE_DIR/stdmatt";
    local RESULT=$(cat "$PATH_TO_INSTALL" | grep "source $SHARE_DIR/manpdf-aliases.sh");

    if [ -z "$RESULT" ]; then
        echo "## stdmatt's manpdf ##"              >> "$PATH_TO_INSTALL";
        echo "source $SHARE_DIR/manpdf-aliases.sh" >> "$PATH_TO_INSTALL";
    fi;

    echo "Done...";
}


##------------------------------------------------------------------------------
ensure_install_directory()
{
    ##
    ## bin's directory.
    if [ ! -d "$INSTALL_BIN_DIR" ]; then
        pw_log_warning                                                             \
            "The installation directory ($INSTALL_BIN_DIR) doesn't exists."        \
            "This indeed shows that it might be not in the PATH variable as well." \
            "manpdf will fails to execute."                                        \
            ""                                                                     \
            "A directory will be created at ($INSTALL_BIN_DIR) automatically, but" \
            "make sure to add the ($INSTALL_BIN_DIR) to your path."                \
            ""                                                                     \
            "PATH=\$PATH:$INSTALL_BIN_DIR"                                         \
            ""                                                                     ;

        local SUDO=$(pw_get_sudo_path);
        $SUDO mkdir -pv "$INSTALL_BIN_DIR";
    fi;

    ##
    ## share's directory.
    if [ ! -d "$INSTALL_SHARE_DIR/stdmatt" ]; then
        local SUDO=$(pw_get_sudo_path);
        $SUDO mkdir -pv "$INSTALL_SHARE_DIR/stdmatt";
    fi;
}

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
echo "[Installing manpdf]";

##
## Make sure that we have the shellscript_utls lib.
if [ ! -e "$PW_SHELLSCRIPT_UTILS_DIR" ]; then
    echo "Error - The stdmatt's shellscript_utils library wasn't found at path:";
    echo "    ($PW_SHELLSCRIPT_UTILS_DIR)."
    echo "";
    echo "This library is required to use manpdf, so you need to install it first.";
    echo "You can find it at the following url:";
    echo "    (https://gitlab.com/stdmatt-libs/shellscript_utils)";
    echo "";
    echo "Aborting...";

    exit 1;
fi;
source "$PW_SHELLSCRIPT_UTILS_DIR";


##
## Check if the user has permission to write on the install directories.
pw_can_write_at_path "$INSTALL_BIN_DIR";
test $? != 0 &&  \
    pw_log_fatal "Can't write at path ($INSTALL_BIN_DIR)";

pw_can_write_at_path "$INSTALL_SHARE_DIR";
test $? != 0 &&  \
    pw_log_fatal "Can't write at path ($INSTALL_SHARE_DIR)";


##
## Install the files.
ensure_install_directory;
SCRIPT_DIR=$(pw_get_script_dir);
SRC_DIR="$SCRIPT_DIR/src";

cp -fv "$SRC_DIR/manpdf.sh"         "$INSTALL_BIN_DIR/manpdf";
ln -fv "$INSTALL_BIN_DIR/manpdf"    "$INSTALL_BIN_DIR/pypdf";
cp -fv "$SRC_DIR/manpdf-aliases.sh" "$INSTALL_SHARE_DIR/stdmatt/manpdf-aliases.sh";

chmod 755 "$INSTALL_BIN_DIR/manpdf";
chmod 755 "$INSTALL_BIN_DIR/pypdf";

##
## Add a entry on the .bash_rc / .bash_profile so we can use the gosh.
DEFAULT_BASH_RC=$(pw_get_default_bashrc_or_profile);
USE_BASH_RC=$(pw_getopt_exists "$@" "--bashrc");
USE_BASH_PROFILE=$(pw_getopt_exists $@ "--bash-profile");

USER_HOME=$(pw_find_real_user_home);
if [ -n "$USE_BASH_RC" ]; then
    _install_source_on "$USER_HOME/.bashrc";
elif [ -n "$USE_BASH_PROFILE" ]; then
    _install_source_on "$USR_HOME/.bash_profile";
else
    _install_source_on $DEFAULT_BASH_RC;
fi
